<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpdatabase' );

/** MySQL database username */
define( 'DB_USER', 'alex' );

/** MySQL database password */
define( 'DB_PASSWORD', '123123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jIeH!GHlq#0/:B5`,jdAeW)U>}%v;uh=;aD;(Y/LLPm-4cQF`*uJx L46ea4VsPo' );
define( 'SECURE_AUTH_KEY',  'vz~SM;,NDV`d& Y/Lyy8]rhI6&l-~Aqygvaj(Bz!%9B%|6x%%)1 Cy@*+EUvu-& ' );
define( 'LOGGED_IN_KEY',    '03JWt2w]F#/B[F2W{<|V.Gr2W7Ra~pb1NB)SD01.>1}}%F6A/uw0>B/3d!|IM)<y' );
define( 'NONCE_KEY',        'ugg:eT?XQok 4i_& lfWmR:_O{V~ZSYPahC_!8F$c>$SA35x!AFCCArc:pZo`V|x' );
define( 'AUTH_SALT',        'ZK~v-m];1XEzghxaN(JlB@z{F(B>J^vaFiKfQ,j5SO-Y5.;.**50t|?vA9SAz+5z' );
define( 'SECURE_AUTH_SALT', '|Y [>^]Q8Ekr-UGOQY7PL3&9=;{o$x`5@q_a8}eFCH//z=T7yWJi&]ttHLCK5C:=' );
define( 'LOGGED_IN_SALT',   '<f!,N}UZ<p_k[iQyT$|Mz,<pb;Y}$cf)8[,0fwrR3qanu>i.740i{ddr=6:M9ET]' );
define( 'NONCE_SALT',       'l;1!8lR(Tev>eW{/j4%|8{&,Eq#e(pn:488G<v{e92~Xr(jb`X]ati,82z^0^8(|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
